// scroll Icon
$(window).scroll(function () {
	if ($(this).scrollTop() < 100) {
		$('.scrollIcon').fadeIn();
	} else {
		$('.scrollIcon').fadeOut();
	}
});
$(document).on('click', '.btn-top', function () {
	$('html, body').animate({
		scrollTop: 0
	}, 400);
	return false;
});


$(document).ready(function(){

    // slidemenu
    $('#asideOpen').click(function(){
        $('#asidebar').stop().animate({ left: "0"}, 300);
		console.log("a");
    });
    $('#asideClose').click(function(){
        $('#asidebar').stop().animate({ left: "-120%"}, 300);
	});
		

	// 200710 시행일자별 이용약관 Select Box 추가 (iframe src change )
	$('#termSelect').change(function() {
		var option = '../html/termAgree_' + $('select').val() + '.html';
		$('#termPanel').attr('src', option);
		});	

	// 200710 메인 alert 메시지 닫기
	$('#alert-close').click(function(){
		$('.top_alert_wrap').hide();
	});  

	// accordion - 요약용 UI
    var $toggleItems = $('.accordion-title');
    var $lastToggleItem = null;	
	
	$toggleItems.off('click').on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
	
	  	var $this = $(this).closest('.accordion-title');
		if( $this.is($lastToggleItem) && $this.hasClass("active") ) {
			$this.removeClass("active");
			$this.next().removeClass('show');
			$this.next().slideUp(200);			
            return;
		} else {
			$this.addClass('active');
			$this.parent().parent().find('.accordion-content').removeClass('show');
			$this.parent().parent().find('.accordion-content').slideUp(200);
			$this.next().toggleClass('show');
			$this.next().slideToggle(200);
		}

        if( $lastToggleItem !== null ) {
            $lastToggleItem.removeClass("active");
		}

        $this.addClass('active');
        $lastToggleItem = $this;
	 });	
    
	/* 전체동의 체크박스 */
	var $checkAll = $('.forms-agreements .all-agree-check input[type=checkbox]');
	$checkAll.on('click', function(e) {
		var $checkboxes = $('.forms-agreements .item-agreement input[type=checkbox]');
		if ($(this).is(':checked')) {
			$checkboxes.prop('checked', true);
			$checkAll.parent().addClass('checked');
			$('.btn-layerClose').addClass('checked');

		} else {
		  $('.forms-agreements .list-agreements input[type=checkbox]').prop('checked', false);
			$checkAll.parent().removeClass('checked');
			$('.btn-layerClose').removeClass('checked');
		}

	});
		$('.btn-layerClose').on('click', function(e){
			if($checkAll.is(':checked')){
				$(this).parents('.dim-layer').fadeOut();
				$('body').removeClass('agreePopup');
			};
	});
  
	/* 약관동의 체크박스 */
	$checkAll.each(function(e) {
		var $localAll = $(this);
		var $checkboxes = $('.forms-agreements .item-agreement input[type=checkbox]');
		$checkboxes.on('click', function(e) {
			if ($checkboxes.filter(':checked').length == $checkboxes.length) {
				$localAll.prop('checked', true);
				$checkAll.parent().addClass('checked');
				$('.btn-layerClose').addClass('checked');
			}
			else {
				$checkAll.prop('checked', false);
				$checkAll.parent().removeClass('checked');
				$('.btn-layerClose').removeClass('checked');
			}
		});
   
	  });



	  $('.popup-modal-dismiss').click(function(){
		$(this).parents('.layout-popup-modal').fadeOut();
		$('body').removeClass('agreePopup');
	  });
	/*
	  $('.btn-view').click(function(){
        var $href = $(this).attr('href');
        layer_popup($href);
	});*/

	$('.popup-modal-01').click(function(){
        $("#popup-modal-01").fadeIn();
    });
	$('.popup-modal-02').click(function(){
        $("#popup-modal-02").fadeIn();
    });
	$('.popup-modal-03').click(function(){
        $("#popup-modal-03").fadeIn();
    });


	function layer_popup(el){
        var $el = $(el);   
		$el.fadeIn();
		$('body').addClass('agreePopup');
	}

});

